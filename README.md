#README#

This is a template of ARC-enabled Cocos2d project.
Cloning this project will save time to manually enable ARC for Cocos2d Projects in XCode

Clone Instructions:

1. Go to your repository folder
-	$ cd ~/repositoryfolder
2. Clone from bitbucket
-	$ git clone https://senarydev@bitbucket.org/senarydev/cocos2d-arc-template.git
3. Enter password at Prompt


###Uploaded By: Rafael###
###rafael.ds@senarysoft.com###
